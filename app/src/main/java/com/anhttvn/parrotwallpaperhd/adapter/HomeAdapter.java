package com.anhttvn.parrotwallpaperhd.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.anhttvn.parrotwallpaperhd.R;
import com.anhttvn.parrotwallpaperhd.model.Photo;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
  private Context mContext;
  private List<Photo> listImage;
  private OnclickImage mOnclick;
  private boolean mConnect;
  private static final int LAYOUT_ADS = 1;
  private static final int LAYOUT_WALLPAPER = 3;


  public HomeAdapter(Context context, List<Photo> list, boolean connect, OnclickImage click) {
    mContext = context;
    listImage = list;
    mOnclick = click;
    mConnect = connect;
  }
  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if (viewType == LAYOUT_ADS) {
      View view = LayoutInflater.from(mContext)
              .inflate(R.layout.ads,parent,false);
      return new ViewHolderAds(view);
    } else {
      View view = LayoutInflater.from(mContext)
              .inflate(R.layout.adapter_home,parent,false);
      return new ViewHolderWallpaper(view);
    }
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    if (holder.getItemViewType() == LAYOUT_ADS) {
      ViewHolderAds holderAds = (ViewHolderAds) holder;
      isBannerADS(holderAds.ads);
    } else {
      Photo photo = listImage.get(position);
      ViewHolderWallpaper holderWallpaper = (ViewHolderWallpaper) holder;
      if (photo == null) {
        return;
      }
//      if (mConnect) {
//        holderWallpaper.warming.setVisibility(View.GONE);
//      }
      holderWallpaper.countView.setText(countView(photo.getView()));
      holderWallpaper.countLike.setText(countView(photo.getLike()));
      if (photo.getPath() == null || photo.getPath().isEmpty()) {
        holderWallpaper.item.setImageResource(R.drawable.ic_no_thumbnail);
      } else {
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Picasso.with(mContext).load(photo.getPath())
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_no_thumbnail)
                .into(holderWallpaper.item);
      }
      holderWallpaper.item.setTag(position);
      holderWallpaper.item.setOnClickListener(this);
    }

  }

  public void isBannerADS (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
           .build();
    ads.loadAd(adRequest);
  }

  private String countView(int count) {
    return count < 1000 ? String.valueOf(count) : (count / 1000) + "K+";
  }



  @Override
  public int getItemCount() {
    return listImage.size();
  }

  @Override
  public int getItemViewType(int position) {
    return listImage.get(position).isAds() ? LAYOUT_ADS : LAYOUT_WALLPAPER;
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.item:
        mOnclick.selectedPosition(position);
        break;

    }
  }


  public class ViewHolderWallpaper extends RecyclerView.ViewHolder {
    private ImageView item;
    private TextView countView, countLike, warming;
    private ImageView imgFavorite;
    public ViewHolderWallpaper(View view) {
      super(view);
      item = view.findViewById(R.id.item);
      countView = view.findViewById(R.id.countView);
      imgFavorite = view.findViewById(R.id.imgFavorite);
//      warming = view.findViewById(R.id.noInternet);
      countLike = view.findViewById(R.id.numberLike);
    }
  }

  public class ViewHolderAds extends RecyclerView.ViewHolder {
    private AdView ads;

    public ViewHolderAds(View view) {
      super(view);
      ads = view.findViewById(R.id.ads);
    }
  }

  public interface OnclickImage {
    void selectedPosition(int position);
  }
}

