package com.anhttvn.parrotwallpaperhd.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.parrotwallpaperhd.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class WallpaperOfflineAdapter extends RecyclerView.Adapter<WallpaperOfflineAdapter.WallpaperOfflineViewHolder> implements View.OnClickListener {

  private List<String> images;
  private EventOnclick click;
  private Context mContext;
  private String typeAdapter;
  public WallpaperOfflineAdapter(Context context, List<String> list, String type, EventOnclick onClick) {
    super();
    images = list;
    click = onClick;
    mContext = context;
    typeAdapter = type;
  }

  @NonNull
  @Override
  public WallpaperOfflineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.adapter_home,parent,false);
    WallpaperOfflineViewHolder itemView = new WallpaperOfflineViewHolder(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull WallpaperOfflineViewHolder holder, int position) {
    holder.view.setVisibility(View.GONE);
    if (typeAdapter.compareToIgnoreCase("FOLDER") == 0) {
      File imgFile = new File(images.get(position));
      if(imgFile.exists())
      {
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        holder.image.setImageBitmap(myBitmap);
      }
    } else {
      InputStream inputstream= null;
      try {
        inputstream = mContext.getAssets().open(images.get(position));
      } catch (IOException e) {
        e.printStackTrace();
      }
      Drawable drawable = Drawable.createFromStream(inputstream, null);
      holder.image.setImageDrawable(drawable);
    }

    holder.image.setOnClickListener(this);
    holder.image.setTag(position);
  }


  @Override
  public int getItemCount() {
    return images.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.item:
        notifyDataSetChanged();
        click.clickWallpaperOffline(position);
        break;
    }
  }

  public class WallpaperOfflineViewHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private RelativeLayout view;
    public WallpaperOfflineViewHolder(@NonNull View itemView) {
      super(itemView);
      image = itemView.findViewById(R.id.item);
      view = itemView.findViewById(R.id.view);
    }
  }

  public interface EventOnclick {
    void clickWallpaperOffline(int position);
  }
}
