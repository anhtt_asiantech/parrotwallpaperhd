package com.anhttvn.parrotwallpaperhd.database;

public class ConfigData {
  public static final String DATABASE = "ParrotWallpaper.db";
  public static final int VERSION = 4;
  public static final String TABLE = "Wallpaper";

  // detail table
  public static final String TITLE = "title";
  public static final String PATH = "path";
  public static final String LIKE ="like";
  public static final String VIEW ="view";
  public static final String ID ="id";
  public static final String ADS ="ads";
  public static final String IS_LIKE = "is_like";

  // create table
  public static final String CREATE_TABLE =
          "CREATE TABLE " + TABLE + " (" +
                  ID + " VARCHAR(100) PRIMARY KEY," +
                  TITLE + " VARCHAR(1000)," +
                  PATH +" VARCHAR(1000)," +
                  LIKE + " INTEGER, " +
                  VIEW +" INTEGER, " +
                  ADS +" INTEGER, " +
                  IS_LIKE +" INTEGER)";


  //delete table
  public static final String DELETE_TABLE =
          "DROP TABLE IF EXISTS " + TABLE;
}