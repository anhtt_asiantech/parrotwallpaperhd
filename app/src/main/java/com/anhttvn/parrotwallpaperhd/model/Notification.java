package com.anhttvn.parrotwallpaperhd.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Notification implements Serializable {

  public String date;
  public String id;
  public String contentEn;
  public String contentVi;

}
