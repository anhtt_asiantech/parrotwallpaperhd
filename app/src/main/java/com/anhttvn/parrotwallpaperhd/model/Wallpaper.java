package com.anhttvn.parrotwallpaperhd.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Wallpaper implements Serializable {

  public String id;
  public String titleEn;
  public String titleVn;
  public String image;
  public String date;
  public String path;
  public int total;
  public boolean ads;
}
