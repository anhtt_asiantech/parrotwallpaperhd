package com.anhttvn.parrotwallpaperhd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.parrotwallpaperhd.MainActivity;
import com.anhttvn.parrotwallpaperhd.R;
import com.anhttvn.parrotwallpaperhd.adapter.HomeAdapter;
import com.anhttvn.parrotwallpaperhd.adapter.WallpaperOfflineAdapter;
import com.anhttvn.parrotwallpaperhd.databinding.ActivityHomeBinding;
import com.anhttvn.parrotwallpaperhd.model.Photo;
import com.anhttvn.parrotwallpaperhd.model.Wallpaper;
import com.anhttvn.parrotwallpaperhd.util.BaseActivity;
import com.anhttvn.parrotwallpaperhd.util.Config;
import com.anhttvn.parrotwallpaperhd.util.MessageEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HomeActivity extends BaseActivity implements
        HomeAdapter.OnclickImage, WallpaperOfflineAdapter.EventOnclick, PopupMenu.OnMenuItemClickListener {
  private ActivityHomeBinding homeBinding;
  private HomeAdapter adapter;
  private List<Photo> photos = new ArrayList<>();
  private boolean flagKey = false;
  private WallpaperOfflineAdapter wallpaperOfflineAdapter;
  private ArrayList<String> wallpaperOffline = new ArrayList<>();
  private Config.TYPE_WALLPAPER typeWallpaper = Config.TYPE_WALLPAPER.FOLDER;
  @Override
  public void init() {
    databaseReference = FirebaseDatabase.getInstance().getReference();
    this.config();
    this.getData();
    this.isBannerADS(homeBinding.ads);
//    this.sortWallpaper();
  }

  protected void config() {
    homeBinding.data.getRoot().setVisibility(View.GONE);
    homeBinding.list.setVisibility(View.GONE);

    // event click back
    homeBinding.header.left.setOnClickListener(v -> {
      finish();
    });

  }

  protected void sortWallpaper() {
    homeBinding.header.right.setOnClickListener(view -> {
      PopupMenu popup = new PopupMenu(this, view);
      popup.setOnMenuItemClickListener(HomeActivity.this);
      popup.inflate(R.menu.sort_menu);
      popup.show();
    });

  }

  @Override
  public View contentView() {
    homeBinding = ActivityHomeBinding.inflate(getLayoutInflater());
    return homeBinding.getRoot();
  }

  private void getData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    Config.KEY key = (Config.KEY) bundle.getSerializable(Config.KEY_MAIN);
    homeBinding.header.title.setText(commonTitleHeader(key));
//    homeBinding.header.right.setVisibility(View.GONE);
    switch (key) {
      case WALLPAPER:
        if (isConnected()) {
//          homeBinding.header.right.setVisibility(View.VISIBLE);
//          homeBinding.header.right.setImageResource(R.drawable.ic_sort);
          homeBinding.note.setVisibility(View.GONE);
          flagKey = true;
          this.photos = new ArrayList<>();
          this.photos = this.loadData();
        } else {
          // connect asset file offline
          // wallpaper offline
          this.typeWallpaper = Config.TYPE_WALLPAPER.GALLERY;
          this.wallpaperOffline = new ArrayList<>();
          wallpaperOffline = wallpaperOffline("wallpaper");
          homeBinding.note.setVisibility(View.VISIBLE);
          homeBinding.note.setText(R.string.wallpaper_message_offline);
          homeBinding.note.setSelected(true);
          adapterWallpaperOffline(this.wallpaperOffline);
        }
        break;
      case FAVORITE:
        this.typeWallpaper = Config.TYPE_WALLPAPER.FAVORITE;
        flagKey = false;
        homeBinding.note.setVisibility(View.GONE);
        this.loadDataFavorite();
        break;

      case DOWNLOAD:
        this.flagKey = false;
        this.typeWallpaper = Config.TYPE_WALLPAPER.FOLDER;
        this.wallpaperOffline = new ArrayList<>();
        homeBinding.note.setVisibility(View.GONE);
        this.wallpaperOffline = this.getWallpaperFolder();
        homeBinding.progress.getRoot().setVisibility(View.GONE);
        adapterWallpaperOffline(this.wallpaperOffline);
        break;
    }
  }

  private void adapter(List<Photo> list) {
    if (list.size() > 0) {
      homeBinding.data.getRoot().setVisibility(View.GONE);
      homeBinding.list.setVisibility(View.VISIBLE);
      adapter = new HomeAdapter(this, list, isConnected(), this);
      GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
      gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
          return adapter.getItemViewType(position) == 1 ?  3 : 1;
        }
      });

      homeBinding.list.setLayoutManager(gridLayoutManager);
      homeBinding.list.setItemAnimator(new DefaultItemAnimator());
      homeBinding.list.setAdapter(adapter);
      adapter.notifyDataSetChanged();
    } else {
      homeBinding.data.getRoot().setVisibility(View.VISIBLE);
      homeBinding.list.setVisibility(View.GONE);
    }
  }

  private void adapterWallpaperOffline(List<String> list) {
    if (list != null && list.size() > 0) {
      homeBinding.data.getRoot().setVisibility(View.GONE);
      homeBinding.list.setVisibility(View.VISIBLE);
      String type = "GALLERY";
      if (this.typeWallpaper == Config.TYPE_WALLPAPER.FOLDER ) {
        type = "FOLDER";
      }
      wallpaperOfflineAdapter = new WallpaperOfflineAdapter(
              this, list, type ,this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
      homeBinding.list.setLayoutManager(layoutManager);
      homeBinding.list.setItemAnimator(new DefaultItemAnimator());
      homeBinding.list.setAdapter(wallpaperOfflineAdapter);
      wallpaperOfflineAdapter.notifyDataSetChanged();
    } else {
      homeBinding.data.getRoot().setVisibility(View.VISIBLE);
      homeBinding.list.setVisibility(View.GONE);
    }
    homeBinding.progress.getRoot().setVisibility(View.GONE);
  }

  private List<Photo> loadData()  {
    homeBinding.progress.getRoot().setVisibility(View.VISIBLE);
    List<Photo> list = new ArrayList<>();
    DatabaseReference ref2 =  databaseReference.child(Config.KEY_WALLPAPER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Photo photo = dsp.getValue(Photo.class);
          if (!db.isWallpaper(photo.getId())) {
            photo.setFavorite(false);
            db.addWallpaper(photo, true);
          }
          list.add(photo);
        }
        Collections.reverse(list);
        homeBinding.progress.getRoot().setVisibility(View.GONE);
        adapter(list);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        homeBinding.progress.getRoot().setVisibility(View.GONE);
//        list = db.listWallpaper();
//        Collections.reverse(listImage);
//        if (listImage.size() > 0) {
//          adapter(listImage);
//        }
      }
    });

    return list;
  }

  protected void loadDataFavorite() {
    this.photos = new ArrayList<>();
    this.photos = db.likePhotos();
    homeBinding.progress.getRoot().setVisibility(View.GONE);
    this.adapter(this.photos);
  }

  @Override
  public void selectedPosition(int position) {
    Intent intent = new Intent(this, SetWallpaperActivity.class);
    intent.putExtra(Config.KEY_HOME, Config.WALLPAPER.ONLINE);
    intent.putExtra(Config.KEY_BIND_PHOTO, photos.get(position));
    startActivity(intent);
  }

  @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
  public void onMessageEvent(MessageEvent event) {
    try {
      if (event.action.equalsIgnoreCase(Config.KEY_UPDATE)) {
        if (flagKey) {
          this.photos = db.photos();
          Collections.reverse(this.photos);
          adapter(this.photos);
        } else {
          if (this.typeWallpaper == Config.TYPE_WALLPAPER.FAVORITE) {
            this.photos = db.likePhotos();
            adapter(this.photos);
          }

        }


      }
    }catch (Exception e) {
      showToast("main : " +e );
    }

  }

  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  protected ArrayList<String> wallpaperOffline(String folderPath) {
    ArrayList<String> pathList = new ArrayList<>();
    try {
      String[] files = this.getAssets().list(folderPath);
      for (String name : files) {
        pathList.add(folderPath + File.separator + name);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return pathList;
  }

  @Override
  public void clickWallpaperOffline(int position) {
    Intent intent = new Intent(this, SetWallpaperActivity.class);
    intent.putExtra(Config.KEY_HOME, Config.WALLPAPER.OFFLINE);
    intent.putExtra(Config.KEY_PATH_WALLPAPER_OFFLINE, wallpaperOffline.get(position));
    intent.putExtra(Config.KEY_WALLPAPER_OFFLINE, this.typeWallpaper);
    startActivity(intent);
  }

  @Override
  public boolean onMenuItemClick(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case R.id.sortDate:
        Collections.sort(this.photos, (o1, o2) -> o1.getId().compareTo(o2.getId()));
        adapter(this.photos);
        return true;
      case R.id.sortView:
        Collections.sort(this.photos, (o1, o2) -> o2.getView() - o1.getView());
        adapter(this.photos);
        return true;
      case R.id.sortLike:
        Collections.sort(this.photos, (o1, o2) -> o2.getLike() - o1.getLike());
        adapter(this.photos);
        return true;
      default:
        return false;
    }
  }
}
