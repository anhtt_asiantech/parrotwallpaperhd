package com.anhttvn.parrotwallpaperhd.ui;

import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.parrotwallpaperhd.R;
import com.anhttvn.parrotwallpaperhd.adapter.NotificationAdapter;
import com.anhttvn.parrotwallpaperhd.databinding.ActivityNotificationBinding;
import com.anhttvn.parrotwallpaperhd.model.Notification;
import com.anhttvn.parrotwallpaperhd.util.BaseActivity;
import com.anhttvn.parrotwallpaperhd.util.Config;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity {
  private ActivityNotificationBinding notificationBinding;
  private NotificationAdapter notificationAdapter;
  @Override
  public void init() {
    this.config();
    this.getData();
  }

  protected void getData() {
    ArrayList<Notification> notifications =
            (ArrayList<Notification>)getIntent().getSerializableExtra(Config.KEY_SEND_NOTIFICATION);
    adapter(notifications);

  }

  @Override
  public View contentView() {
    notificationBinding = ActivityNotificationBinding.inflate(getLayoutInflater());
    return notificationBinding.getRoot();
  }

  protected void adapter(List<Notification> notifications) {

    notificationAdapter = new NotificationAdapter(notifications, this);
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
    notificationBinding.list.setLayoutManager(layoutManager);
    notificationBinding.list.setItemAnimator(new DefaultItemAnimator());
    notificationBinding.list.setAdapter(notificationAdapter);
    notificationAdapter.notifyDataSetChanged();
  }

  private void config() {
    notificationBinding.header.right.setVisibility(View.GONE);
    notificationBinding.header.title.setText(getString(R.string.notification));
    notificationBinding.header.left.setOnClickListener(v -> {
      finish();
    });
  }
}
