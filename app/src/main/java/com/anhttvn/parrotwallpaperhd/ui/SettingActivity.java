package com.anhttvn.parrotwallpaperhd.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

import com.anhttvn.parrotwallpaperhd.R;
import com.anhttvn.parrotwallpaperhd.databinding.ActivitySettingBinding;
import com.anhttvn.parrotwallpaperhd.model.Information;
import com.anhttvn.parrotwallpaperhd.util.BaseActivity;
import com.anhttvn.parrotwallpaperhd.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class SettingActivity extends BaseActivity {
  private ActivitySettingBinding settingBinding;
  @Override
  public void init() {
    this.getData();

    settingBinding.header.left.setOnClickListener(v -> {
      finish();
    });
  }

  @Override
  public View contentView() {
    settingBinding = ActivitySettingBinding.inflate(getLayoutInflater());
    return settingBinding.getRoot();
  }

  private void getData() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    Config.SETTING_KEY key = (Config.SETTING_KEY) bundle.getSerializable(Config.KEY_SETTING);

    switch (key) {
      case PRIVACY:
        settingBinding.privacy.setVisibility(View.VISIBLE);
        settingBinding.information.setVisibility(View.GONE);
        settingBinding.header.title.setText(R.string.title_privacy);
        this.showPrivacy();
        isBannerADS(settingBinding.ads);
        break;
      case INFORMATION:
        databaseReference  = FirebaseDatabase.getInstance().getReference();
        settingBinding.privacy.setVisibility(View.GONE);
        settingBinding.information.setVisibility(View.VISIBLE);
        settingBinding.header.title.setText(R.string.title_information);
        this.getInformation();
        break;
    }
  }

  protected void showPrivacy() {

    ProgressDialog progressDialog = new ProgressDialog(this);
    progressDialog.setMessage("Loading");
    progressDialog.setCancelable(false);
    settingBinding.content.requestFocus();
    settingBinding.content.getSettings().setLightTouchEnabled(true);
    settingBinding.content.getSettings().setJavaScriptEnabled(true);
    settingBinding.content.getSettings().setGeolocationEnabled(true);
    settingBinding.content.setSoundEffectsEnabled(true);
    settingBinding.content.setBackgroundColor(0);
    progressDialog.show();

    settingBinding.content.loadUrl("file:///android_asset/private.html");
    settingBinding.content.requestFocus();
    settingBinding.content.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageFinished(WebView view, String url) {
        progressDialog.dismiss();
      }
    });
  }

  private void getInformation() {
    List<Information> information = new ArrayList<>();
    DatabaseReference ref2 =  databaseReference.child(Config.INFORMATION);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        Information data = dataSnapshot.getValue(Information.class);
        showInformation(data);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        showInformation(null);
      }
    });
  }

  private void showInformation(Information information) {
    if (information == null) {
      settingBinding.numberOnline.setText("N/A");
      settingBinding.numberOffline.setText("N/A");
      settingBinding.dateUpdate.setText("N/A");
      settingBinding.numberVersion.setText("N/A");
      settingBinding.dateUpdateApp.setText("N/A");
    } else {
      settingBinding.numberOnline.setText(information.getWallpaperOnline());
      settingBinding.numberOffline.setText(information.getWallpaperOffline());
      settingBinding.dateUpdate.setText(information.getDateUpdate());
      settingBinding.numberVersion.setText(information.getVersion());
      settingBinding.dateUpdateApp.setText(information.getDateUpdateApp());
    }

  }
}
