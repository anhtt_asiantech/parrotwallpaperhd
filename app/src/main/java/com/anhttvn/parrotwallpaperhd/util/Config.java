package com.anhttvn.parrotwallpaperhd.util;

public class Config {

  /**
   * Forder
   */

  public static String FOLDER_DOWNLOAD = "PARROT_WALLPAPER";

  /**
   * api
   */
  public static String KEY_NOTIFICATION = "Notification";

  public static String KEY_WALLPAPER = "Wallpaper";
  public static final String INFORMATION ="Information";

  /**
   * key
   */
  public static final String KEY_MAIN = "26021990&04061992";
  public static String KEY_BIND_WALLPAPER = "00000";
  public static String KEY_BIND_PHOTO = "11111";
  public static String KEY_SEND_NOTIFICATION = "22222";
  public static String KEY_SETTING = "2210&2303";
  public static String KEY_WALLPAPER_OFFLINE = "2016&2018";
  public static String KEY_PATH_WALLPAPER_OFFLINE = "HungAnh";
  public static String KEY_HOME = "TranTienAnh";



  /**
   * UTIL
   */
  public  static final int PERMISSION_REQUEST_CODE = 7;

  public static final int REQUEST_CODE_REFRESH = 10;

  public static final String KEY_UPDATE = "UPDATE_DATA";


  /**
   * key function
   */

  public enum KEY  {
    WALLPAPER,
    DOWNLOAD,
    FAVORITE,
    NOTIFICATION
  }

  public enum SETTING_KEY {
    PRIVACY,
    INFORMATION,
    SHARE_APP
  }

  public enum WALLPAPER {
    OFFLINE,
    ONLINE
  }

  public enum TYPE_WALLPAPER {
    GALLERY,
    FOLDER,
    FAVORITE
  }
}
